#Einrichtung des Servers

Folgende Programme müssen installiert werden
1. [Node.js](https://nodejs.org/en/)
2. [MongoDB](https://docs.mongodb.com/manual/administration/install-community/)
**Wichtig** Installationsanleitung befolgen, vor Allem den Schritt `Set up the MongoDB environment`.

##Server initialisieren
Im Ordner `tachymeter/server` den Befehl `npm install` ausführen

##Server starten
Im Ordner `tachymeter/server` den Befehl `npm run start` ausführen

##Sonstiges
- ECMAScript 6, strict-mode bevorzugen