/**
 * Created by Indiana on 23.06.2017.
 */
module.exports = {
    // Secret key for JWT
    'secret': '4w3s0m3m0n3ybuddy',
    // Database connection
    'database': 'mongodb://localhost:27017',
    // Setting port for our server
    'port': process.env.PORT || 3000
}

