/**
 * Created by Indiana on 23.06.2017.
 */
"use strict";
// Controllers
const AuthenticationController = require('./controllers/authentication');
const AccountController = require('./controllers/account');
const TransactionController = require('./controllers/transaction');
const UserController = require('./controllers/user');

// Requirements
const express = require('express');
const passport = require('passport');
const config = require('./config/main');

// Bruteforce protection
var ExpressBrute = require('express-brute');
var MongoStore = require('express-brute-mongo');
var MongoClient = require('mongodb').MongoClient;

var bruteforceStore = new MongoStore(function (ready) {
    MongoClient.connect(config.database, function(err, db) {
        if (err) throw err;
        ready(db.collection('bruteforce-store'));
    });
});

var bruteforceOptions = {
    freeRetries: 500,
    minWait: 5*60*1000, // 5 minutes
    maxWait: 60*60*1000 // 1 hour
}

var bruteforce = new ExpressBrute(bruteforceStore, bruteforceOptions);

// Middleware to require login/auth
const requireAuth = passport.authenticate('jwt', { session: false });

module.exports = function(app) {
    // Initializing route groups
    const apiRoutes = express.Router();
    const authRoutes = express.Router();
    const accountRoutes = express.Router();
    const categoryRoutes = express.Router();
    const userRoutes = express.Router();

// Auth Routes
    // Set auth routes as subgroup/middleware to apiRoutes
    apiRoutes.use('/auth', authRoutes);

    authRoutes.post('/register', AuthenticationController.register);

    // Catch unauthorized errors on login routes and authenticate
    authRoutes.post('/login', bruteforce.prevent, function(req, res, next) {
        //look at the 2nd parameter to the below call
        passport.authenticate('local', function(err, user, info) {
            if(!user){
                res.status(403).json(info);
            };
            if(user){
                let userInfo = AuthenticationController.setUserInfo(user);
                res.status(200).json({
                    token: 'JWT ' + AuthenticationController.generateToken(userInfo),
                    user: userInfo
                });
            }
        })(req, res, next);
    });

    // Catch unauthorized errors on account routes
    accountRoutes.use(function(req, res, next) {
        passport.authenticate('jwt', { session: false }, function(err, user, info) {
            if(!user){
                if(info){
                    res.status(401).json(info);
                }
            };
            next();
        })(req, res, next);
    });

// Account Routes
    // Set account routes as a subgroup/middleware to apiRoutes
    apiRoutes.use('/account', accountRoutes);

    // Create new account
    accountRoutes.post('/', requireAuth, AccountController.createAccount);
    // Update a single account
    accountRoutes.put('/:accountId', requireAuth, AccountController.updateAccount);
    // Get a single account
    accountRoutes.get('/:accountId', requireAuth, AccountController.getAccount);
    // Get all accounts
    accountRoutes.get('/', requireAuth, AccountController.getAccounts);
    // Get sharedKey key
    accountRoutes.get('/sharedKey/:accountId', requireAuth, AccountController.getSharedKey);
    // get all the accounts with this shared key
    accountRoutes.get('/sharedKey/list/:sharedKey', requireAuth, AccountController.getAccountsWithKey);
    // get all the accounts with this shared key
    accountRoutes.put('/sharedKey/:sharedKey', requireAuth, AccountController.putUserToAccount);

// Transaction Routes
    // Create new transaction
    accountRoutes.post('/:accountId/transaction', requireAuth, TransactionController.createTransaction);
    // Get all transactions
    accountRoutes.get('/:accountId/transaction', requireAuth, TransactionController.getTransactions);
    // Get a single transaction
    accountRoutes.get('/:accountId/transaction/:transactionId', requireAuth, TransactionController.getTransaction);
    // Update a single transaction
    accountRoutes.put('/:accountId/transaction/:transactionId', requireAuth, TransactionController.updateTransaction);
    // Delete a single transaction
    accountRoutes.delete('/:accountId/transaction/:transactionId', requireAuth, TransactionController.deleteTransaction);
    // Get all categorys with their respective value for an account
    accountRoutes.get('/:accountId/category', requireAuth, TransactionController.getCategorys);

// Category Routes
    // Set account routes as a subgroup/middleware to apiRoutes
    apiRoutes.use('/category', categoryRoutes);

    // Only for dev - Create a new category
    categoryRoutes.post('/', CategoryController.createCategory);
    // Get all categorys
    categoryRoutes.get('/', requireAuth, CategoryController.getCategorys);
    // Get a single category
    categoryRoutes.get('/:categoryId', requireAuth, CategoryController.getCategory);

// User Routes
    // Set account routes as a subgroup/middleware to apiRoutes
    apiRoutes.use('/user', userRoutes);

    // Get the first and last name of the user
    userRoutes.get('/me', requireAuth, UserController.getUser);
    // Get the first and last name from a specific user id
    userRoutes.get('/:userId', requireAuth, UserController.getUsernameFromId);

// Set url for API group routes
    app.use('/api', apiRoutes);
};
