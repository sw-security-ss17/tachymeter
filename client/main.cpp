#include <windows.h>
#include <stdlib.h>
#include <iostream>
#include <limits>
#include <cmath>

using namespace std;

int main() {
    POINT currentPos;
    signed int lastX = 0;
    signed int lastY = 0;
    signed long long int distance = 0;
    signed long long int maximum = numeric_limits<signed long long int>::max();
    GetCursorPos(&currentPos);

    while (1) {
        lastX = currentPos.x;
        lastY = currentPos.y;
        GetCursorPos(&currentPos);

        if (currentPos.x != lastX || currentPos.y != lastY) {
            signed long long int difference = sqrt((currentPos.x - lastX)*(currentPos.x - lastX) + (currentPos.y - lastY) * (currentPos.y - lastY));
            if (distance < (maximum - difference)) {
                distance += difference;
            }
        }
        cout << "\x1b[2J\x1b[H" << flush;
        cout << "Distance Travelled in cm: " << distance / 43 << endl;

        Sleep(50);
    }
    return 0;
}